var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('default', function() {
    console.log('hello');
});

gulp.task('scripts', function() {
    return gulp.src('./Angular_Reference/src/**/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./dist/'));
});